from django.template.loaders.filesystem import Loader as FilesystemLoader
from django.conf import settings

from .loader_utils import get_template_dirs


class Loader(FilesystemLoader):
    def get_dirs(self):
        template_dirs = super(Loader, self).get_dirs()
        if settings.TEMPLATES:
            template_dirs = self.engine.dirs
        elif settings.TEMPLATE_DIRS:
            template_dirs = settings.TEMPLATE_DIRS
        return get_template_dirs(template_dirs)
