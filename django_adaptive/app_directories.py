from django.template.loaders.app_directories import Loader \
    as AppDirectoriesLoader
from .loader_utils import get_template_dirs

try:
    from django.template.loaders.app_directories import app_template_dirs
except ImportError:
    from django.template.utils import get_app_template_dirs
    app_template_dirs = get_app_template_dirs('templates')


class Loader(AppDirectoriesLoader):
    def get_dirs(self):
        return get_template_dirs(app_template_dirs)
